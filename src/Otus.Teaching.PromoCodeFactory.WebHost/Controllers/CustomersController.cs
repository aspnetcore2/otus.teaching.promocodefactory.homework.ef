﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System.Linq;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {

        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;

        public CustomersController(IRepository<Customer> customerRepository, IRepository<Preference> preferenceRepository)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
        }

        /// <summary>
        /// Gets All Customers
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<CustomerResponse>> GetCustomersAsync()
        {
            var customers = await _customerRepository.GetAllAsync();

            var response = customers.Select(customer => new CustomerResponse(customer));

            return Ok(response);
        }

        /// <summary>
        /// Gets Customer By Id
        /// </summary>
        /// <param name="id">Customer Id</param>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerByIdAsync(Guid id)
        {
            if (id == Guid.Empty)
                return BadRequest();

            var customer = await _customerRepository.GetByIdAsync(id);

            if (customer == null)
                return NotFound();

            var res = customer.CustomerPreferences;

            var response = new CustomerResponse(customer);

            return Ok(response);

        }
        /// <summary>
        /// Creates Customer
        /// </summary>        
        /// <param name="request">Create Or Edit Customer request</param>
        /// <returns>Customer Id</returns>
        [HttpPost]
        public async Task<IActionResult> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            Guid Id = Guid.NewGuid();

            Customer customer = new Customer()
            {
                Id = Guid.NewGuid()
                ,
                FirstName = request.FirstName
                ,
                LastName = request.LastName
                ,
                Email = request.Email
            };

            var preferences = await _preferenceRepository.GetRangeByIdsAsync(request.PreferenceIds);

            var customerPreferences = preferences.Select(p => new CustomerPreference() {Customer = customer, Preference = p }).ToList();

            customer.CustomerPreferences = customerPreferences;

            await _customerRepository.AddAsync(customer);

            return Ok(customer.Id);
        }
        /// <summary>
        /// Updates Customer
        /// </summary>
        /// <param name="id">Customer Id</param>
        /// <param name="request">Create Or Edit Customer request</param> 
        /// <returns></returns>
        [HttpPut("{id:guid}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            Customer customer = await _customerRepository.GetByIdAsync(id);

            customer.FirstName = request.FirstName;
            customer.LastName = request.LastName;
            customer.Email = request.Email;

            customer.CustomerPreferences.Clear();

            var preferences = await _preferenceRepository.GetRangeByIdsAsync(request.PreferenceIds);

            var customerPreferences = preferences.Select(p => new CustomerPreference() {Customer = customer, Preference = p }).ToList();

            customer.CustomerPreferences = customerPreferences;

            await _customerRepository.UpdateAsync(customer);

            return Ok();
        }
        /// <summary>
        /// Deletes Customer
        /// </summary>
        /// <param name="id">Customer Id</param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {
            if (id == Guid.Empty)
                return BadRequest();

            var customer = await _customerRepository.GetByIdAsync(id);

            if (customer == null)
                return NotFound();

            await _customerRepository.DeleteAsync(customer);

            return Ok();
        }

    }
}