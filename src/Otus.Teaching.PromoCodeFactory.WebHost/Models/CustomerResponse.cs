﻿using System;
using System.Collections.Generic;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System.Linq;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class CustomerResponse
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        
        public List<PromoCodeShortResponse> PromoCodes { get; set; }

        public List<PreferenceResponse> Preferences { get; set; }

        public CustomerResponse() { }

        public CustomerResponse(Customer customer)
        {
            Id = customer.Id;
            FirstName = customer.FirstName;
            LastName = customer.LastName;
            Email = customer.Email;

            Preferences = customer.CustomerPreferences?.Select(preference => new PreferenceResponse(preference.Preference)).ToList();
            PromoCodes = customer.PromoCodes?.Select(promo => new PromoCodeShortResponse(promo)).ToList();
        }
    }
}