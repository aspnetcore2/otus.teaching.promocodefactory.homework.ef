﻿using System;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class PromoCodeShortResponse
    {
        public Guid Id { get; set; }
        
        public string Code { get; set; }

        public string ServiceInfo { get; set; }

        public string BeginDate { get; set; }

        public string EndDate { get; set; }

        public string PartnerName { get; set; }

        public PromoCodeShortResponse() { }
        public PromoCodeShortResponse(PromoCode promoCode)
        {
            Id = promoCode.Id;
            Code = promoCode.Code;
            ServiceInfo = promoCode.ServiceInfo;
            PartnerName = promoCode.PartnerName;

            BeginDate = promoCode.BeginDate.ToString();
            EndDate = promoCode.EndDate.ToString();
        }
    }
}