﻿using System;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class PreferenceResponse
    {
        public Guid Id { get; set; }
        public string Name { get; set; }

        public PreferenceResponse() { }

        public PreferenceResponse(Preference preference)
        {
            this.Id = preference.Id;
            this.Name = preference.Name;
        }
    }
}
