﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using System.Linq.Expressions;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories
{
    public interface IRepository<T>
        where T: BaseEntity
    {
        Task<IEnumerable<T>> GetAllAsync();
        
        Task<T> GetByIdAsync(Guid id);

        Task AddAsync(T value);
        Task UpdateAsync(T value);
        Task DeleteAsync(T value);
        Task<IEnumerable<T>> GetRangeByIdsAsync(List<Guid> ids);
    }
}