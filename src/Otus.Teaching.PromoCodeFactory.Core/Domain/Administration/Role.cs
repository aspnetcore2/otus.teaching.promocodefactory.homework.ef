﻿using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Administration
{
    /// <summary>
    /// Role
    /// </summary>
    public class Role : BaseEntity
    {      
        public string Name { get; set; }
        public string Description { get; set; }  
        public virtual ICollection<Employee> Employees { get; set; }
    }
}