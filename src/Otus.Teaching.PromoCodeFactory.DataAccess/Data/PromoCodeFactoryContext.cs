﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public class PromoCodeFactoryContext : DbContext
    {
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Preference> Preferences { get; set; }
        public DbSet<PromoCode> PromoCodes { get; set; }

        public PromoCodeFactoryContext(DbContextOptions<PromoCodeFactoryContext> options)
        : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //Primary keys
            modelBuilder.Entity<Employee>().HasKey(e => e.Id);
            modelBuilder.Entity<Role>().HasKey(r => r.Id);
            modelBuilder.Entity<Customer>().HasKey(r => r.Id);
            modelBuilder.Entity<Preference>().HasKey(p => p.Id);
            modelBuilder.Entity<PromoCode>().HasKey(p => p.Id);
            modelBuilder.Entity<CustomerPreference>().HasKey(cp => cp.Id);

            //Properties settings

            modelBuilder.Entity<Employee>().Property(e => e.FirstName).HasMaxLength(255);
            modelBuilder.Entity<Employee>().Property(e => e.LastName).HasMaxLength(255);
            modelBuilder.Entity<Employee>().Property(e => e.Email).HasMaxLength(255);

            modelBuilder.Entity<Role>().Property(r => r.Name).HasMaxLength(255);
            modelBuilder.Entity<Role>().Property(r => r.Description).HasMaxLength(255);

            modelBuilder.Entity<Customer>().Property(r => r.FirstName).HasMaxLength(255);
            modelBuilder.Entity<Customer>().Property(r => r.LastName).HasMaxLength(255);
            modelBuilder.Entity<Customer>().Property(r => r.Email).HasMaxLength(255);

            modelBuilder.Entity<Preference>().Property(p => p.Name).HasMaxLength(255);

            modelBuilder.Entity<PromoCode>().Property(p => p.Code).HasMaxLength(255);
            modelBuilder.Entity<PromoCode>().Property(p => p.ServiceInfo).HasMaxLength(255);
            modelBuilder.Entity<PromoCode>().Property(p => p.PartnerName).HasMaxLength(255);

            //References

            modelBuilder.Entity<CustomerPreference>()
                .HasOne(cp => cp.Customer)
                .WithMany(p => p.CustomerPreferences)
                .HasForeignKey(cp1 => cp1.CustomerId);

            modelBuilder.Entity<CustomerPreference>()
                .HasOne(cp => cp.Preference)
                .WithMany(p => p.CustomerPreferences)
                .HasForeignKey(cp1 => cp1.PreferenceId);

            modelBuilder.Entity<PromoCode>()
                .HasOne(promo => promo.Customer)
                .WithMany(c => c.PromoCodes)
                .HasForeignKey(promo => promo.CustomerId);

            modelBuilder.Entity<Employee>()
                        .HasOne(p => p.Role)
                        .WithMany(p => p.Employees)
                        .HasForeignKey(p => p.RoleId);

            //Init

            modelBuilder.Entity<Role>().HasData(FakeDataFactory.Roles);
            modelBuilder.Entity<Employee>().HasData(FakeDataFactory.Employees);
            modelBuilder.Entity<Customer>().HasData(FakeDataFactory.Customers);
            modelBuilder.Entity<Preference>().HasData(FakeDataFactory.Preferences);

            base.OnModelCreating(modelBuilder);
        }
    }
}
